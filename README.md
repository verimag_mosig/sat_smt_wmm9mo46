# SAT and SMT solving (WMM9MO46)

by David Monniaux

**emails** firstname.name@imag.fr

Please clone this repository and keep it synchronized when teachers update it.

## Contents

1. **26 sep** Propositional logic, NNF, CNF / DNF
2. **03 oct** BDDs
3. **10 oct** BDDs, hash-consing
4. **17 oct** reachability analysis using BDDs, Tseitin's encoding
5. **24 oct** pure literals, resolution, quantifier elimination by resolution, DPLL, CDCL, UIPs 

